<?php

namespace classes;

use PHPMailer\PHPMailer\PHPMailer;

class SendForm
{
    /**
     * Client name.
     *
     * @var string
     */
    private $name;

    /**
     * Client email.
     *
     * @var string
     */
    private $email;

    /**
     * Recaptcha token.
     *
     * @var string
     */
    private $recaptcha;

    /**
     * SendForm constructor.
     *
     * @param string $name
     * @param string $email
     * @param string $recaptcha
     */
    function __construct(string $name, string $email, string $recaptcha)
    {
        $this->name = $name;
        $this->email = $email;
        $this->recaptcha = $recaptcha;
    }

    /**
     * Send mail.
     *
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function send()
    {
        // Validate data.
        if (!$this->validate($this->name, $this->email)) {
            // Set old form data.
            $_SESSION['name'] = $this->name;
            $_SESSION['email'] = $this->email;

            return;
        }

        // Validate recaptcha.
        if (!$this->validateCaptcha($this->recaptcha)) {
            // Set old form data.
            $_SESSION['name'] = $this->name;
            $_SESSION['email'] = $this->email;

            return;
        }

        // Set up mailer.
        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $mail->isSMTP();
        $mail->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = 'ssl';
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465;
        $mail->Username ='your-mail@gmail.com';
        $mail->Password = 'your-password';

        //Set who the message is to be sent from
        $mail->setFrom('exabyt3r@gmail.com', 'Fernando López Moreno');
        // Add address.
        $mail->AddAddress('exabyt3r@gmail.com');

        // Content
        $mail->isHTML(true);
        $mail->Subject = 'Example PHPMailer';
        $mail->Body    = "<p><strong>Nombre:</strong> {$this->name}</p> <p><strong>Correo electrónico:</strong> {$this->email}</p>";

        //send the message, check for errors
        if (!$mail->send()) {
            error_log($mail->ErrorInfo);
        } else {
            // Set success message.
            $_SESSION['message'] = 'success';
        }
    }

    /**
     * Validate email data.
     *
     * @param string $name
     * @param string $email
     * @return bool
     */
    public function validate(string $name, string $email)
    {
        // Validate name.
        if (strlen($name) < 5) {
            $_SESSION['message'] = 'error-name';
            return false;
        }

        // Validate email.
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $_SESSION['message'] = 'error-email';
            return false;
        }

        return true;
    }

    /**
     * Recaptcha additional validation process.
     *
     * @param string $recaptcha
     * @return bool
     */
    public function validateCaptcha(string $recaptcha)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data = array(
            'secret' => '6LeqKHwUAAAAAJI679BSatXkvZ_heWyIJg2XJU8X',
            'response' => $recaptcha
        );
        $options = array(
            'http' => array (
                'header' => 'Content-Type: application/x-www-form-urlencoded\r\n',
                'method' => 'POST',
                'content' => http_build_query($data)
            )
        );
        $context  = stream_context_create($options);
        $verify = file_get_contents($url, false, $context);
        $captcha_success = json_decode($verify);
        if ($captcha_success->success === false) {
            $_SESSION['message'] = 'error-captcha';
            return false;
        }

        return true;
    }
}