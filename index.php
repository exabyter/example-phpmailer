<?php
session_start();
session_unset();

// Autoload composer.
require 'vendor/autoload.php';

use classes\SendForm;

// Set current URL.
$currentUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
// Clear session errors.
$_SESSION['message'] = null;

if (isset($_POST['name']) && isset($_POST['email'])) {
    // Prevent cross-Site scripting.
    $_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    // Create new send form instance.
    $form = new SendForm($_POST['name'], $_POST['email'], $_REQUEST['g-recaptcha-response']);

    // Send form.
    $form->send();
}

?>
<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sample form using PHPMailer</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/node_modules/bootstrap/dist/css/bootstrap.min.css">

    <!-- Recaptcha -->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <div class="container mt-5">
        <div class="row d-flex justify-content-center">
            <div class="col-6">

                <!-- The form -->
                <form method="POST" action="<?php echo $currentUrl; ?>">
                    <!-- Name -->
                    <div class="form-group">
                        <label for="name">Nombre:</label>
                        <input type="text" id="name" class="form-control" name="name" placeholder="Escriba su nombre" value="<?php echo isset($_SESSION['name']) ? $_SESSION['name'] : '' ?>" required>
                    </div><!-- ./Name -->
                    <!-- Email -->
                    <div class="form-group">
                        <label for="email">Correo electrónico:</label>
                        <input type="text" id="email" class="form-control" name="email" placeholder="Escriba su correo electrónico" value="<?php echo isset($_SESSION['email']) ? $_SESSION['email'] : '' ?>" required>
                    </div><!-- ./Email -->
                    <div class="form-group my-3">
                        <div class="g-recaptcha" data-sitekey="6LeqKHwUAAAAAAvR61l8Oi8KqY6nsDcx7F1ImdYD"></div>
                    </div>
                    <button type="submit" class="btn btn-primary">Enviar</button>
                </form><!-- ./The form -->

                <?php if (isset($_SESSION['message'])) : ?>
                    <!-- Form messages -->
                    <div class="my-3">
                        <?php if ($_SESSION['message'] === 'success') : ?>
                            <!-- Success message -->
                            <div class="alert alert-success" role="alert">
                                ¡Información enviada correctamente!
                            </div><!-- ./Success message -->
                        <?php elseif ($_SESSION['message'] === 'error-email') : ?>
                            <!-- Error message -->
                            <div class="alert alert-danger" role="alert">
                                El correo electrónico no es válido.
                            </div><!-- ./Error message -->
                        <?php elseif ($_SESSION['message'] === 'error-name') : ?>
                            <!-- Error message -->
                            <div class="alert alert-danger" role="alert">
                                El nombre es demasiado corto.
                            </div><!-- ./Error message -->
                        <?php elseif ($_SESSION['message'] === 'error-captcha') : ?>
                            <!-- Error message -->
                            <div class="alert alert-danger" role="alert">
                                Por favor valide el recaptcha.
                            </div><!-- ./Error message -->
                        <?php endif; ?>
                    </div><!-- ./Form messages -->
                <?php endif; ?>
            </div>
        </div>
    </div>
</body>
</html>