# Example PHPMailer

Developed by @exabyter.

## Instructions for installation

1. Install PHP dependencies.

    `$ composer install`
    
2. Install Frontend dependencies.

    `$ npm install`
    
3. Configure your gmail account on SendForm class line 84 & 85.
    
    ```php
    $mail->Username ='your-mail@gmail.com';
    $mail->Password = 'your-password';
    ```
